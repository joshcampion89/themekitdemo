#ThemeKit

Thank you for using ThemeKit.

For details on how to include the libraries directly, please read [iOS 9 Universal Cocoa Touch Frameworks by kodmunki](https://kodmunki.wordpress.com/2015/09/22/ios-9-universal-cocoa-touch-frameworks/).

To use the framework using cocoapods:

